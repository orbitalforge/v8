V8 JavaScript Engine
=============

This is a binary release of Google's V8 JavaScript Engine.

V8 Project page: https://github.com/v8/v8/wiki
